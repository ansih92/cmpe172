<?php
    require_once 'db-login.php';
    $conn = new mysqli($hn, $un, $pw, $db);
    if ($conn->connect_error) die($conn->connect_error);

    if(isset($_POST["emp_name"])){
        $emp_name = explode(" ", $_POST["emp_name"]);
        $target_first_name = $emp_name[0];
        $target_last_name = $emp_name[1];
    }

    $query = "SELECT employees.*, titles.title FROM employees INNER JOIN titles ON employees.emp_no = titles.emp_no WHERE employees.first_name = '$target_first_name' AND employees.last_name = '$target_last_name';";
    $result = $conn->query($query);
    if (!$result) die($conn->error);
    if ($result->num_rows > 0) {
        echo '<table style="width:100%"> 
            <tr> 
                <th>emp_no</th> 
                <th>birth_date</th> 
                <th>first_name</th> 
                <th>last_name</th> 
                <th>gender</th> 
                <th>hire_date</th> 
                <th>title</th>
            </tr>';
        while($row = $result->fetch_assoc()) {
            $emp_no = $row["emp_no"];
            $birth_date = $row["birth_date"];
            $first_name = $row["first_name"];
            $last_name = $row["last_name"];
            $gender = $row["gender"];
            $hire_date = $row["hire_date"];
            $title = $row["title"];
            echo("<tr> 
                <td>$emp_no</td> 
                <td>$birth_date</td> 
                <td>$first_name</td> 
                <td>$last_name</td> 
                <td>$gender</td> 
                <td>$hire_date</td>
                <td>$title</td>
            </tr>");
        }
    }
?>

<html>
    <style>
        table, th, td {
            border: 1px solid black;
            text-align:center;
        }
    </style>
   <body>
      <form action = "" method = "POST" enctype = "multipart/form-data">
         Employee name: <input type="text" name="emp_name"><br>
         <input type = "submit"/>
      </form>
   </body>
</html>