<?php
    require_once 'db-login.php';
    require_once 'TwitterAPIExchange.php';
    
    $conn = new mysqli($hn, $un, $pw, $db);
    if ($conn->connect_error) die($conn->connect_error);

    if(isset($_POST["emp_no"])){
        $emp_no = $_POST["emp_no"];
    }

    if(isset($_POST["emp_title"])){
        $emp_title = $_POST["emp_title"];
        if ($emp_title !== '') {
            $query = "UPDATE titles SET title='$emp_title' WHERE emp_no=$emp_no";
            $result = $conn->query($query);
            $query = "SELECT first_name, last_name FROM employees WHERE emp_no=$emp_no";
            $result = $conn->query($query);
            while($row = $result->fetch_assoc()) {
                $first_name = $row["first_name"];
                $last_name = $row["last_name"];
            }
            postToTwitter("$first_name $last_name was just promoted to $emp_title");
        }
    }

    if(isset($_POST["emp_salary"])){
        $emp_salary = $_POST["emp_salary"];
        if ($emp_salary !== '') {
            $query = "UPDATE salaries SET salary=$emp_salary WHERE emp_no=$emp_no";
            $result = $conn->query($query);
        }
    }

    function postToTwitter($msg) {
        $settings = array(
            'oauth_access_token' => "625448859-W8bxEXgl9j4EBdYZXCvrnAUWI4VTO4OwIeJjugHf",
            'oauth_access_token_secret' => "zLWg68fUFQHI1OVKofya8Q3vR9NoxG0r14uhTs7OmEQL6",
            'consumer_key' => "BUo2i2DGfvgre5ZG7amnQJwuW",
            'consumer_secret' => "CKR3aVJlKNFWr8BNKayy16Ir4NkfEUzqntvDKlcpfTAOAnbKTi"
        );

        $url = 'https://api.twitter.com/1.1/statuses/update.json';
        $requestMethod = 'POST';

        $postfields = array(
            'status' => $msg
        );

        $twitter = new TwitterAPIExchange($settings);
        $twitter->buildOauth($url, $requestMethod)
             ->setPostfields($postfields)
             ->performRequest();
    }

    function getAvailibleTitles($conn) {
        $query = "SELECT DISTINCT title from titles";
        $result = $conn->query($query);
        $availibleTitles = [];
        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
                array_push($availibleTitles, $row["title"]);
            }
        }
        return $availibleTitles;  
    }
?>

<html>
    <style>
        
    </style>
   <body>
      <form action = "" method = "POST" enctype = "multipart/form-data">
        <p>Edit Employee.</p><br>
         Employee no (required): <input type="text" name="emp_no"><br>
         Employee Title (optional): <input type="text" name="emp_title"><br>
         Employee Salary (optional): <input type="text" name="emp_salary"><br>
         <input type = "submit"/>
      </form>
   </body>
</html>